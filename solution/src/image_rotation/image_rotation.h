#ifndef IMAGE_ROTATION_H
#define IMAGE_ROTATION_H
#include "../image_manager/image_manager.h"
#include <stdbool.h>
bool rotate_image(const struct image old, struct image* const new);
#endif //IMAGE_ROTATION_H
