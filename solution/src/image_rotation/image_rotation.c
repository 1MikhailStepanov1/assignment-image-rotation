#include "image_rotation.h"

bool rotate_image(const struct image old, struct image* const new){
    const size_t new_height = old.width;
    const size_t new_width = old.height;
    if (new->content != NULL){
        image_delete(*new);
    }
    *new = image_create(new_height, new_width);
    for (size_t i = 0; i < new_height; i++){
        for (size_t j = 0; j < new_width; j++){
            struct pixel temp = pixel_get(old, j, i);
            pixel_set(*new, temp, i, new->width - 1 - j);
        }
    }
    return true;
}
