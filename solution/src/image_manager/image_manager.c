#include "image_manager.h"


struct image image_create(const size_t height, const size_t width){
    struct image image = {0};
    image.height = height;
    image.width = width;
    image.content = malloc(height * width * sizeof(struct pixel));
    return image;
}

void image_delete(const struct image image){
    free(image.content);
}
size_t image_calculate_size(const struct image image){
    return image.height * image.width * sizeof(struct pixel);
}

struct pixel pixel_get(const struct image image, const size_t row, const size_t column){
    return image.content[row*image.width + column];
}

bool pixel_set(const struct image image, const struct pixel pixel, const size_t row, const size_t column){
    bool isSuccess = false;
    if ((image.height > row) && (image.width > column)){
        isSuccess = true;
        image.content[row*image.width + column] = pixel;
    }
    return isSuccess;
}
