#ifndef IMAGE_MANAGER
#define IMAGE_MANAGER

#include <malloc.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

struct image {
    size_t height;
    size_t width;
    struct pixel* content;
};

struct pixel{
    uint8_t b;
    uint8_t g;
    uint8_t r;
};

size_t image_calculate_size(const struct image image);
struct image image_create(const size_t height, const size_t width);
void image_delete(struct image image);
struct pixel pixel_get(const struct image image, const size_t row, const size_t column);
bool pixel_set(struct image image, const struct pixel pixel, const size_t row, const size_t column);
#endif //IMAGE_MANAGER
