#ifndef BMP_HEADER_H
#define BMP_HEADER_H

#pragma pack(push, 1)
struct bmp_header
{
    uint16_t type;
    uint32_t fileSize;
    uint32_t reserved;
    uint32_t offset;
    uint32_t headerSize;
    uint32_t width;
    uint32_t height;
    uint16_t planes;
    uint16_t bit_count;
    uint32_t compression;
    uint32_t imageSize;
    uint32_t pixelPerMeterX;
    uint32_t pixelPerMeterY;
    uint32_t colorsUsed;
    uint32_t colorsImportant;
};
#pragma pack(pop)

#endif //BMP_HEADER_H
