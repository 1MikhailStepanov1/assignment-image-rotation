#include "bmp_writer.h"
#define MAX_AMOUNT_OF_PADDINGS 3
static const struct bmp_header header_blank = {
        .type = 19778,
        .reserved = 0,
        .offset = sizeof(struct bmp_header),
        .headerSize = 40,
        .planes = 1,
        .compression = 0,
        .pixelPerMeterX = 2834,
        .pixelPerMeterY = 2834,
        .colorsUsed = 0,
        .colorsImportant = 0,
        .bit_count = 24
};

static struct bmp_header create_header(const struct image image){
    struct bmp_header new_header = header_blank;
    size_t image_size = image_calculate_size(image);
    new_header.imageSize = image_size;
    new_header.height = image.height;
    new_header.width = image.width;
    new_header.fileSize = sizeof(struct bmp_header) + image_size;
    return new_header;
}

static enum status_code write_header(struct image image, FILE* const file){
    struct bmp_header header = create_header(image);
    if (fwrite(&header, sizeof(struct bmp_header), 1, file) != 1) return FILE_WRITE_ERROR;
    return SUCCESS;
}

static enum status_code write_content(struct image image, FILE* const file){
    const uint8_t padding = get_padding(image.width);
    const size_t height = image.height;
    const size_t width = image.width;
    const uint8_t paddings[MAX_AMOUNT_OF_PADDINGS] = {0};
    for (size_t i = 0; i < height; i++){
        if (fwrite(image.content + i*width, sizeof(struct pixel)*width, 1, file) == 0) return FILE_WRITE_ERROR;
        if ((fwrite(paddings, padding, 1, file) == 0) && padding != 0) return FILE_WRITE_ERROR;
    }
    return SUCCESS;
}

enum status_code to_bmp(struct image image, FILE* const file){

    enum status_code header_write_result = write_header(image, file);

    if (header_write_result != SUCCESS) return header_write_result;

    enum status_code content_write_result = write_content(image, file);

    if (content_write_result != SUCCESS) return content_write_result;

    return SUCCESS;
}
