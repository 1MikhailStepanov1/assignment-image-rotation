#include "bmp_reader.h"



static enum status_code read_header(FILE* const file, struct bmp_header* const header){
    if (fread(header, sizeof(struct bmp_header), 1, file) == 1){
        return SUCCESS;
    } else return FILE_READ_HEADER_ERROR;
}


static enum status_code read_content(FILE* file, struct image* const image){
    const uint8_t padding = get_padding(image->width);
    struct pixel* content = image->content;
    const size_t height = image->height;
    const size_t width = image->width;
    for (size_t i = 0; i < height; i++){
        if (fread(content + width*i, sizeof(struct pixel), width, file) != width) return FILE_READ_ERROR;
        if (fseek(file, padding, SEEK_CUR) != 0) return BMP_FORMAT_READ_ERROR;
    }
    return SUCCESS;
}


enum status_code from_bmp(FILE* const file, struct image* image){
    struct bmp_header bmp_header = {0};
    enum status_code header_from_file = read_header(file, &bmp_header);
    if (header_from_file != SUCCESS){
        return header_from_file;
    }
    *image = image_create(bmp_header.height, bmp_header.width);
    enum status_code read_content_status_code = read_content(file, image);
    if (read_content_status_code != SUCCESS){
        return read_content_status_code;
    }
    return SUCCESS;
}
