#ifndef BMP_PADDING_H
#define BMP_PADDING_H
#include "../image_manager/image_manager.h"
#include <stdint.h>
uint8_t get_padding(const uint32_t width);
#endif //BMP_PADDING_H
