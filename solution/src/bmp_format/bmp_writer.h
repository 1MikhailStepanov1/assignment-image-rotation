#ifndef BMP_WRITER_H
#define BMP_WRITER_H
#include "../image_manager/image_manager.h"
#include "../util/util.h"
#include "bmp_header.h"
#include "bmp_padding.h"
#include "malloc.h"
#include <stdint.h>
#include <stdio.h>



enum status_code to_bmp(struct image image, FILE* file);
#endif //BMP_WRITER_H
