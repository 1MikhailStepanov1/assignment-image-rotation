#ifndef BMP_READER_H
#define BMP_READER_H

#include "../image_manager/image_manager.h"
#include "../util/util.h"
#include "bmp_header.h"
#include "bmp_padding.h"
#include "malloc.h"
#include <stdint.h>
#include <stdio.h>



enum status_code from_bmp(FILE* const file, struct image* image);
#endif //BMP_READER_H
