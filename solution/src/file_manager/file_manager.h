#ifndef FILE_MANAGER
#define FILE_MANAGER

#include "../bmp_format/bmp_reader.h"
#include "../bmp_format/bmp_writer.h"
#include "../util/util.h"
#include <errno.h>
#include <stdbool.h>
#include <stdio.h>

bool read_image(const char* const filename, struct image* image);
bool write_image(const char* const filename, struct image image);
#endif //FILE_MANAGER
