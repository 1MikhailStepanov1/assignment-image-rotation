#include "file_manager.h"


bool read_image(const char* const filename, struct image* const image){
    FILE* file = NULL;
    file = fopen(filename, "rb");
    if (file == NULL) {
        print_status_code_message(FILE_OPEN_ERROR);
        return false;
    }
    enum status_code read_bmp = from_bmp(file, image);
    if (read_bmp != SUCCESS){
        print_status_code_message(read_bmp);
        return false;
    }
    fclose(file);
    print_status_code_message(SUCCESS);
    return true;
}

bool write_image(const char* const filename, struct image const image){
    FILE* file = NULL;
    file = fopen(filename, "wb");
    if (errno != 0) {
        print_status_code_message(FILE_OPEN_ERROR);
        return false;
    }
    enum status_code write_status = to_bmp(image,file);
    if (write_status != SUCCESS){
        print_status_code_message(write_status);
        return false;
    }
    fclose(file);
    if (errno != 0) {
        print_status_code_message(FILE_CLOSE_ERROR);
        return false;
    }
    return true;
}
