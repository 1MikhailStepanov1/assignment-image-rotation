#include "file_manager/file_manager.h"
#include "image_manager/image_manager.h"
#include "image_rotation/image_rotation.h"
#include <stdio.h>

int main( int argc, char** argv ) {
//    setvbuf(stdout, NULL, _IONBF, 0);
//    (void) argc; (void) argv; // supress 'unused parameters' warning
    if (argc != 3){
        fprintf(stderr, "%s", "Incorrect number of arguments.");
        return 0;
    }
    bool isSuccess = true;
    const char* input_file = argv[1];
    const char* output_file = argv[2];
    struct image old_image = {0};
    struct image new_image = {0};
    isSuccess = read_image(input_file, &old_image);
    if (!isSuccess) return 0;
    isSuccess = rotate_image(old_image, &new_image);
    if (!isSuccess) return 0;
    isSuccess = write_image(output_file, new_image);
    if (!isSuccess) return 0;
    image_delete(old_image);
    image_delete(new_image);
    return 0;
}
