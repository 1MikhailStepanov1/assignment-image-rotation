#ifndef STATUS_CODES
#define STATUS_CODES

enum status_code {
    SUCCESS,
    FILE_OPEN_ERROR,
    FILE_READ_HEADER_ERROR,
    FILE_READ_ERROR,
    BMP_FORMAT_READ_ERROR,
    FILE_WRITE_ERROR,
    FILE_CLOSE_ERROR
};

void print_status_code_message(enum status_code statusCode);
#endif //STATUS_CODES
