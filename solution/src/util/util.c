#include "stdio.h"
#include "util.h"

static const char* const status_code_messages[]={
        [SUCCESS] = "Success.",
        [FILE_OPEN_ERROR] = "File can't be opened.",
        [FILE_READ_HEADER_ERROR] = "Header of file can't be read.",
        [FILE_READ_ERROR] = "File can't be read.",
        [BMP_FORMAT_READ_ERROR] = "Program can't read info from bmp format.",
        [FILE_WRITE_ERROR] = "Program can't write info into file.",
        [FILE_CLOSE_ERROR] = "File can't be closed."
};

void print_status_code_message(enum status_code statusCode){
    printf("%s \n", status_code_messages[statusCode]);
}
